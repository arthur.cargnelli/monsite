<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(["middleware" => 'logger' ], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/portfolio',array('uses' => 'portfolio@show'))->name("portfolio");

    Route::get('/albums',array('uses' => 'albums@show'))->name("albums");

    Route::get('/news',array('uses' => 'newsController@show'))->name("news");

    Route::get('/contact',function(){
        return view('contact');
    })->name('contact');
    Route::post('/contact/send',array('uses' => 'mailer@send'))->name("send");
    Route::get('/portfolio/CV', array('uses'=> 'portfolio@getCV'))->name('getCV');
    Route::get('/logout', array('uses'=> 'HomeController@logout'))->name('logout');
});
    Route::post('/contact/send',array('uses' => 'mailer@send'))->name("send");

    Route::get('/portfolio/formations', array('uses'=> 'portfolio@getFormations'))->name('getFormation');
    Route::get('/portfolio/experiences', array('uses'=> 'portfolio@getExperiences'))->name('getExperience');
    Route::get('/portfolio/projects', array('uses'=> 'portfolio@getProjects'))->name('getProjects');
    Route::get('/portfolio/competences', array('uses'=> 'portfolio@getCompetences'))->name('getCompetences');
    Route::get('/getALbums', array('uses'=> 'albums@getAlbums'))->name('getAlbums');
    Route::get('/album/{id}', array('uses'=> 'albums@getAlbum'))->name('getAlbum');
    Route::get('/photos/{id}/getPhotos', array('uses'=> 'photos@getPhotos'))->name('getPhotos');
    Route::get('/portfolio/CV', array('uses'=> 'portfolio@getCV'))->name('getCV');
    //news
    Route::get('/news/categories',array('uses' => 'newsController@getNewsCategories'))->name("getNewsCategories");
    Route::get('/news/get',array('uses' => 'newsController@getNews'))->name("getNews");
    Route::post('/news/add',array('uses' => 'newsController@addNews'))->name("addNews");

    Auth::routes([
        'register' => false,
        'logout' => false
    ]);
    Route::get('/logout', array('uses'=> 'HomeController@logout'))->name('logout');


// ADMIN ROUTES

Route::group(['prefix' => 'admin'], function()
{
    Route::get('/', array('uses' => 'adminController@home'))->name('AdminHome');

    // Links
    Route::get('/links', array('uses' => 'adminController@link'))->name('AdminLink');
    Route::get('/links/new', array('uses' => 'admin\linkAdminController@new'))->name('newLink');
    Route::get('/links/edit/{id}', array('uses' => 'admin\linkAdminController@edit'))->name('editLink');
    Route::post('/link/update/{id}', array('uses' => 'admin\linkAdminController@update'))->name('updateLink');
    Route::post('/link/create', array('uses' => 'admin\linkAdminController@create'))->name('createLink');
    Route::delete('/links/delete/{id}', array('uses' => 'admin\linkAdminController@delete'))->name('deleteLink');

    // Album
    Route::get('/albums', array('uses' => 'adminController@album'))->name('AdminAlbum');
    Route::get('/albums/new', array('uses' => 'admin\albumAdminController@new'))->name('newAlbum');
    Route::get('/albums/edit/{id}', array('uses' => 'admin\albumAdminController@edit'))->name('editAlbum');
    Route::post('/albums/update/{id}', array('uses' => 'admin\albumAdminController@update'))->name('updateAlbum');
    Route::post('/albums/create', array('uses' => 'admin\albumAdminController@create'))->name('createAlbum');
    Route::delete('/albums/delete/{id}', array('uses' => 'admin\albumAdminController@delete'))->name('deleteAlbum');
    Route::put('/albums/show/{id}', array('uses' => 'admin\albumAdminController@editShow'))->name('editShowAlbum');

    // Route::get('/admin/competences/new', array('uses' => 'admin\albumAdminController@new'))->name('newAlbum');

    // Photo
    Route::get('/photos', array('uses' => 'adminController@photo'))->name('AdminPhoto');
    Route::get('/photos/new', array('uses' => 'admin\photoAdminController@new'))->name('newPhoto');
    Route::get('/photos/edit/{id}', array('uses' => 'admin\photoAdminController@edit'))->name('editPhoto');
    Route::post('/photos/update/{id}', array('uses' => 'admin\photoAdminController@update'))->name('updatePhoto');
    Route::post('/photos/create', array('uses' => 'admin\photoAdminController@create'))->name('createPhoto');
    Route::delete('/photos/delete/{id}', array('uses' => 'admin\photoAdminController@delete'))->name('deletePhoto');

    // Experiences
    Route::get('/experiences', array('uses' => 'adminController@experience'))->name('AdminExperience');
    Route::get('/experiences/new', array('uses' => 'admin\experienceAdminController@new'))->name('newExperience');
    Route::get('/experiences/edit/{id}', array('uses' => 'admin\experienceAdminController@edit'))->name('editExperience');
    Route::post('/experiences/update/{id}', array('uses' => 'admin\experienceAdminController@update'))->name('updateExperience');
    Route::post('/experiences/create', array('uses' => 'admin\experienceAdminController@create'))->name('createExperience');
    Route::delete('/experiences/delete/{id}', array('uses' => 'admin\experienceAdminController@delete'))->name('deleteExperience');

    // Formations
    Route::get('/formations', array('uses' => 'adminController@formation'))->name('AdminFormation');
    Route::get('/formations/new', array('uses' => 'admin\formationAdminController@new'))->name('newFormation');
    Route::get('/formations/edit/{id}', array('uses' => 'admin\formationAdminController@edit'))->name('editFormation');
    Route::post('/formations/update/{id}', array('uses' => 'admin\formationAdminController@update'))->name('updateFormation');
    Route::post('/formations/create', array('uses' => 'admin\formationAdminController@create'))->name('createFormation');
    Route::delete('/formations/delete/{id}', array('uses' => 'admin\formationAdminController@delete'))->name('deleteFormation');

    // Projects
    Route::get('/projects', array('uses' => 'adminController@project'))->name('AdminProject');
    Route::get('/projects/new', array('uses' => 'admin\projectAdminController@new'))->name('newProject');
    Route::get('/projects/edit/{id}', array('uses' => 'admin\projectAdminController@edit'))->name('editProject');
    Route::post('/projects/update/{id}', array('uses' => 'admin\projectAdminController@update'))->name('updateProject');
    Route::post('/projects/create', array('uses' => 'admin\projectAdminController@create'))->name('createProject');
    Route::delete('/projects/delete/{id}', array('uses' => 'admin\projectAdminController@delete'))->name('deleteProject');
    Route::put('/projects/show/{id}', array('uses' => 'admin\projectAdminController@editShow'))->name('editShowProject');

    // Compétences
    Route::get('/competences', array('uses' => 'adminController@competence'))->name('AdminCompetence');
    Route::get('/competences/new', array('uses' => 'admin\competenceAdminController@new'))->name('newCompetence');
    Route::get('/competences/edit/{id}', array('uses' => 'admin\competenceAdminController@edit'))->name('editCompetence');
    Route::post('/competences/update/{id}', array('uses' => 'admin\competenceAdminController@update'))->name('updateCompetence');
    Route::post('/competences/create', array('uses' => 'admin\competenceAdminController@create'))->name('createCompetence');
    Route::delete('/competences/delete/{id}', array('uses' => 'admin\competenceAdminController@delete'))->name('deleteCompetence');

    // Users
    Route::get('/users', array('uses' => 'admin\userAdminController@list'))->name('AdminUsers');
    Route::get('/users/new', array('uses' => 'admin\userAdminController@new'))->name('newUser');
    Route::get('/users/edit/{id}', array('uses' => 'admin\userAdminController@edit'))->name('AdminUserProfile');
    Route::post('/users/update/{id}', array('uses' => 'admin\userAdminController@update'))->name('updateUser');
    Route::post('/users/update/{id}/password', array('uses' => 'admin\userAdminController@updatePassword'))->name('updateUserPassword');
    Route::post('/users/create', array('uses' => 'admin\userAdminController@create'))->name('createUser');
    Route::delete('/users/delete/{id}', array('uses' => 'admin\userAdminController@delete'))->name('deleteUser');

    // Contacts
    Route::get('/contacts', array('uses' => 'adminController@contact'))->name('AdminContact');
    Route::get('/contacts/{id}', array('uses' => 'admin\contactAdminController@show'))->name('AdminShowContact');
    Route::delete('/contacts/delete/{id}', array('uses' => 'admin\contactAdminController@delete'))->name('deleteContact');

    // news
    Route::get('/news', array('uses' => 'adminController@news'))->name('AdminNews');
    Route::delete('/news/delete/{id}', array('uses' => 'admin\newsAdminController@delete'))->name('deleteNews');

    // news Categories
    Route::get('/news/categories', array('uses' => 'adminController@newsCategory'))->name('AdminNewsCategory');
    Route::get('/news/categories/new', array('uses' => 'admin\newsCategoriesAdminController@new'))->name('newCategory');
    // Route::get('/links/edit/{id}', array('uses' => 'admin\newsCategoriesAdminController@edit'))->name('editLink');
    // Route::put('/link/update/{id}', array('uses' => 'admin\newsCategoriesAdminController@update'))->name('updateLink');
    Route::post('/news/categories/add', array('uses' => 'admin\newsCategoriesAdminController@create'))->name('createNewCategory');
    Route::delete('/news/categories/delete/{id}', array('uses' => 'admin\newsCategoriesAdminController@delete'))->name('deleteNews');

    // Logs
    Route::get('/logs', array('uses' => 'adminController@logs'))->name('AdminLogsCategory');
    Route::delete('/logs/delete/{id}', array('uses' => 'admin\logsAdminController@delete'))->name('deleteLogs');

});
