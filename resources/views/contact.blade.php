@extends('baseTemplate')
@section('tags')
    <script src="https://www.google.com/recaptcha/api.js?render=6Leh098UAAAAAM-tdhoUg4TV5HStJm2or_wsRsM8"></script>
@endsection
@section('title','Contact')

@section('content')
    <div class="container">
        <h1 class="text_center mt-5p"> Contact </h1>
        <div class="contact-form d-flex justify-content-center">
            <form action="/contact/send" method="post">
                @csrf
                <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" /><br >
                <div id="contact">
                    <div class="row">
                        <input type="text" class="form-control" name="name" placeholder="Name"/>
                    </div>
                    <div class="row">
                        <input type="email" class="form-control" name="email" placeholder="Email"/>
                    </div>
                    <div class="row">
                        <textarea name="body" class="form-control" placeholder="Body"></textarea>
                    </div>
                    <div class="row submit">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection
    @section('script')
    <script>
    grecaptcha.ready(function() {
    grecaptcha.execute('6Leh098UAAAAAM-tdhoUg4TV5HStJm2or_wsRsM8', {action: 'homepage'})
    .then(function(token) {
        //console.log(token);
        document.getElementById('g-recaptcha-response').value=token;
        });
    });
    </script>
    @endsection

