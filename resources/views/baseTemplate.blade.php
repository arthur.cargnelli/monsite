<!DOCTYPE html>
<html lang="fr">
    <head>
        <link rel="icon" type="image/png" href="/favicon.ico">
        <!-- ========== Meta Tags : Init. ========== -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- ========== Meta Tags : Responsive. ========== -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- ========== Meta Tags : Standard. ========== -->
        <meta name="owner" content="Arthur CARGNELLI">
        <link type="text/plain" rel="author" href="https://old.arthurcargnelli.fr/humans.txt" />
        <meta name="author" content="Arthur CARGNELLI">
        <meta name="title" content="Arthur Cargnelli">
        <meta name="keywords" content="dev, web, front, back, voyage, nouvelle-calédonie, international, partage, photo, bordeaux, iut, informatique, plaisir, divertissement, vuejs, php, ruby"/>
        <meta name="description" content="Originaire de Nouvelle-Calédonie, j'ai fait des études de développement web en France à Bordeaux. J'aime particulièrement voyager et la photographie . "/>
        <!-- ========== Meta Tags : Facebook OpenGraph. ========== -->
        <meta property="og:type" content="website">
        <meta property="og:locale" content="fr_fr">
        <meta property="og:title" content="Arthur Cargnelli"/>
        <meta property="og:description" content="">
        <meta property="og:site_name" content="Arthur Cargnelli"/>
        <meta property="og:image" content=""/>
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:see_also" content="https://twitter.com/ArthurKagy">
        <!-- ========== Meta Tags : Twitter Card. ========== -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@ArthurKagy">
        <meta name="twitter:creator" content="@ArthurKagy">
        <meta name="twitter:title" content="Arthur Cargnelli">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- ========== Meta Tags : Google Verification. ========== -->
        <meta name="google-site-verification" content=""/>
        <!-- ========== Meta Tags : Google Robots. ========== -->
        <meta name="robots" content="all">
        <!-- ========== Title ========== -->
        <title>@yield('title') | Arthur Cargnelli</title>

        <!-- ========== Humans.txt authorship http://humanstxt.org ========== -->
        <link type="text/plain" rel="author" href="{{ env('APP_URL') }}humans.txt">
        <!-- ========== Fonts ========== -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> -->

        <meta charset="UTF-8">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- Custom styles for this template -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ env('APP_URL') }}css/app.css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://ackee.arthurcargnelli.fr/tracker_cargnelli.js" data-ackee-server="https://ackee.arthurcargnelli.fr" data-ackee-domain-id="fa8e25a9-3768-44be-aa3f-444d8936eb13"></script>

        @yield('css')

        @yield('tags')

    </head>
    <body id="body" class="loading">
        <!--LOADER-->
        <div id="loading" class="loader"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
        <!--END LOADER-->
        <div class="wrapper">
            <!--HEADER-->
            <header id="header" class="header navbar navbar-expand-md sticky_header">
                <div class="container">
                    <a class="navbar-brand logo" href="/">
                    <h1 id="brand-logo"> Arthur Cargnelli </h1>
                    </a>

                    <button class="navbar-toggler" id="header_icon" onclick="menu(event)" type="button">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div class="flex-sm-row-reverse collapse navbar-collapse " id="headernav">
                    <ul class=" nav navbar-nav menu" id="menu-mobile">
                        <!-- {# MENU #} -->
                        <li class="nav-item d-flex align-items-center hide">
                            <h2> Arthur Cargnelli</h2>
                        </li>
                        <li class="nav-item d-flex align-items-center">
                            <a class="nav-link" href="/"> Accueil </a>
                        </li>
                        <li class="nav-item d-flex align-items-center">
                            <a class="nav-link" href="/portfolio"> Portfolio </a>
                        </li>
                        <li class="nav-item d-flex align-items-center">
                            <a class="nav-link" href="/albums"> Photo </a>
                        </li>
                        <li class="nav-item d-flex align-items-center">
                            <a class="nav-link" href="/news"> Actus </a>
                        </li>
                        @if (Auth::check())
                            <li class="nav-item d-flex align-items-center">
                            <a class="nav-link" href="/admin"> Admin </a>
                            </li>
                            <li class="nav-item d-flex align-items-center hide">
                            <a class="nav-link" href="/contact"> Contact </a>
                            </li>
                        @endif
                        <li class="nav-item d-flex align-items-center hide">
                            <a class="nav-link" href= "https://gitlab.com/arthur.cargnelli"> Profil Gitlab </a>
                        </li>
                        <li class="nav-item d-flex flex-row hide">
                            <a class="icon" href="https://twitter.com/ArthurKagy"><img src="/img/SVG/twitter.svg" alt="Twitter"/></a>
                            <a class="icon" href="https://www.facebook.com/arthur.cargnellikagy"><img src="/img/SVG/facebook.svg" alt="Facebook" /></a>
                        </li>
                        <li class="nav-item d-flex flex-row hide">
                            <a class="icon" href="https://www.instagram.com/tuthur9/"><img src="img/SVG/instagram.svg" alt="Instagram" /></a>
                            <a class="icon" href="https://www.linkedin.com/in/arthurcargnellikagy/"><img src="/img/SVG/linkedin.svg" alt="Linkedin" /></a>
                        </li>
                    </ul>
                    </div>
                </div>
            </header>
            <!--END HEADER-->

            <!-- Page Content -->
            <div class ="page_content">
            {{-- read and display just one flash message type  --}}
                @include('layouts/flash-msg')

                @yield('content')

                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-md">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="modal-title text-center">Site en développement</h4>
                        </div>
                        <div class="modal-body">
                        <p> N'hésitez pas à me contacter, via la page de <a href="/contact">contact</a> ou par whatsapp, si vous avez des suggestions et / ou si il y a un bug sur mon site ! </p>
                        <p> Vous pouvez également voir les différents projets que j'ai effectué pour le moment sur gitlab : <a href="https://gitlab.com/arthur.cargnelli"> arthur.cargnelli </a> </p>
                        </div>
                        <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->

                <!--FOOTER-->
            <footer id="footer" class="footer header navbar sticky_header">
                <div class="container ">

                    <!-- {# Footer nav #} -->
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-7 col-12 ml-3p">
                        <div class="footer_box mt20">
                        <!-- {# Title nav #} -->
                        <div class="footer_header">
                            <h4 class="footer_title">
                            Arthur CARGNELLI
                            </h4>
                        </div>

                        <!-- {# Links #} -->
                        <div class="footer_box_body">
                            <ul class="flex-wrap flex-column">
                            <!-- {# Réseau sociaux #} -->
                            <li class="nav-item d-flex flex-row">
                                <a class="icon" href="https://twitter.com/ArthurKagy"><img src="https://old.arthurcargnelli.fr//img/SVG/twitter.svg" alt="Twitter"/></a>
                                <a class="icon" href="https://www.facebook.com/arthur.cargnellikagy"><img src="https://old.arthurcargnelli.fr//img/SVG/facebook.svg" alt="Facebook" /></a>
                                <a class="icon" href="https://www.instagram.com/tuthur9/"><img src="https://old.arthurcargnelli.fr/img/SVG/instagram.svg" alt="Instagram" /></a>
                                <a class="icon" href="https://www.linkedin.com/in/arthurcargnellikagy/"><img src="https://old.arthurcargnelli.fr//img/SVG/linkedin.svg" alt="Linkedin" /></a>
                            </li>
                            </ul>
                        </div>
                        </div>
                    </div>

                    <!-- {# Contact section #} -->
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4 col-12">
                        <div class="footer_box contactBox">
                        <div class="footer_header">
                            <!-- {# Title #} -->
                            <h4 class="footer_title">
                            Me contacter ?
                            </h4>

                        </div>

                        <!-- {# Email #} -->
                        <div class="footer_box_body">
                            <a href="/contact"> Contact</a>
                        </div>
                        <div class="elfsight-app-fd4dc081-b3fe-425c-b353-8c3123962dff"></div>
                    </div>
                    </div>

                <hr class="w-100 footerhr">
                <!-- {# Copyright section #} -->
                <div class="row justify-content-center w-100 mt-1 mb-3">
                    <div class="col-md-12 col-12">
                    <div class="text-center footer_box_body">
                        <a href="https://www.paypal.me/CARGNELLIArthur">Faire un don avec <!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tbody><tr><td align="center"><a href="https://www.paypal.me/CARGNELLIArthur" title="PayPal Comment Ca Marche" onclick="javascript:window.open('https://www.paypal.me/CARGNELLIArthur','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo/pp_cc_mark_37x23.jpg" border="0" alt="PayPal Logo" /></a></td></tr></tbody></table><!-- PayPal Logo --> </a>
                    </div>
                    <p class="text-center">
                        Copyright &copy;
                        {{-- {# Display current date #} --}}
                        {{ date('Y') }}
                        &nbsp;| All rights reserved |   <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a>
                    </p>
                    </div>
                </div>
            </footer>
            <!--END FOOTER-->
        </div>
    </body>
    <!-- jQuery -->
    <script src="{{ env('APP_URL') }}vendor/jquery/jquery.min.js"></script>
    <!-- JS -->
    <script src="{{ env('APP_URL') }}vendor/popper/umd/popper.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ env('APP_URL') }}vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    {{-- Axio --}}
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- ========== WhatsApp ========== -->
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
    @yield('script')
    <script>
        function menu(e){
            e.preventDefault();
            console.log("in function menu toggle")
            $('#headernav').toggleClass('collapse');
            $('#menu-mobile').toggleClass('menu-mobile');
        };
        if(!sessionStorage.getItem('shown-modal')){
        $('#myModal').modal('show');
        sessionStorage.setItem('shown-modal','true')
        }
        function onReady(callback) {
            var intervalId = window.setInterval(function() {
            if (document.getElementsByTagName('body')[0] !== undefined) {
                window.clearInterval(intervalId);
                callback.call(this);
            }
            }, 1000);
        }

        function setVisible(selector, visible) {
            var display = visible ? 'block' : 'none';
            $(selector).css('display', display);
        }

        onReady(function() {
            setVisible('.wrapper', true);
            $('body').removeClass('loading');
            setVisible('#loading', false);
        });
    </script>
</html>
