@extends('baseTemplate')
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @section('title', "News")
        <!-- Fonts -->
    </head>
    <body>
        @section('content')
            <div id="app">
                <router-view></router-view>
            </div>
        @endsection
        @section('script')
            <script src="{{ mix('js/news.js') }}"></script>
        @endsection
    </body>
</html>
