@extends('admin.baseAdminTemplate')

@section('title', isset($experience)? 'Edit':"New")

@section('css')
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"rel = "stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nouvel Experience</h3>
    <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form action="{{ isset($experience)? '/admin/experiences/update/'.$experience->id: '/admin/experiences/create'}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Name : </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="name" class="form-control" value="{{isset($experience)? $experience->name:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="location"> Location :</label>
                </div>
                <div class="col-md-12">
                    <input type="text" name="location" class="form-control" value="{{isset($experience)? $experience->location:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="Compagny"> Compagny :</label>
                </div>
                <div class="col-md-12">
                    <input type="text" name="compagny" class="form-control" value="{{isset($experience)? $experience->compagny:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="date_start"> Date de début :</label>
                </div>
                <div class="col-md-12">
                    <input type="text" id="date_start" name="date_start" class="form-control datepicker" value="{{isset($experience)? $experience->date_start:''}}"/>
                </div>
            </div>
            <div class="row" id="date_end">
                <div class="col-md-12">
                    <label for="date_end"> Date de fin :</label>
                </div>
                <div class="col-md-12">
                    <input type="text" name="date_end" class="form-control datepicker" value="{{isset($experience)? $experience->date_end:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(isset($experience) && $experience->current == 1)
                        <input type="checkbox" id="current" name="current" onchange="toggleCurrent()" checked>
                    @else
                        <input type="checkbox" id="current" name="current" onchange="toggleCurrent()">
                    @endif
                    <label for="current"> Current :</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
    {{-- <!-- /.card -->
    <div class="row box-info">
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Total des stopWords</span>
            <span class="info-box-number"> {{ $totalStopWords }}</span>
            </div>
        </div>
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-check-circle"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">StopWords ajouté aujourd'hui</span>
            <span class="info-box-number">{{ $totalToday }}</span>
            </div>
        </div>
    </div>
</div> --}}
@stop
@section('script')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $( function() {
        if(document.getElementById("current").checked == true){
            $("#date_end").hide();
            $("#current").attr('value', true);
        }
        else{
            $("#date_end").show();
            $("#current").attr('value', false);
        }
        $( ".datepicker" ).datepicker();
    } );
    function toggleCurrent(){
        if(document.getElementById("current").checked == true){
            $("#date_end").hide();
            $("#current").attr('value', true);
        }
        else{
            $("#date_end").show();
            $("#current").attr('value', false);
        }
    }
</script>
@stop
