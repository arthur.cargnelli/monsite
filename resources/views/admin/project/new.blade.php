@extends('admin.baseAdminTemplate')

@section('title', isset($project)? 'Edit':"New")

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nouveau Projet</h3>
    <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form action="{{ isset($project)? '/admin/projects/update/'.$project->id: '/admin/projects/create' }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Name : </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="name" class="form-control" value="{{isset($project)? $project->name:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="presentation_image"> Description :</label>
                </div>
                <div class="col-md-12">
                    <textarea name="description" class="form-control" value="{{isset($project)? $project->description:''}}"> {{isset($project)? $project->description:''}} </textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="files">Image : </label>
                </div>
                <div class="col-md-12">
                    <input type="file" name="file" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="files">Url : </label>
                </div>
                <div class="col-md-12">
                    <input type="text" name="url" class="form-control" value="{{isset($project)? $project->url:''}}"/>
                </div>
            </div>
            @if(count($competences) != 0)
                <div class="row" >
                    <div class="col-md-12">
                        <label for="files">Compétences : </label>
                    </div>
                    <div class="col-md-12">
                        <select multiple="multiple" name="competences[]" class="form-control">
                            @foreach($competences as $competence)
                                <option value="{{$competence->id}}">{{$competence->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
            <div class="row" >
                <div class="col-md-12">
                    <label for="files">Afficher : </label>
                </div>
                <div class="col-md-12">
                    <input  type="checkbox" name="show" value="{{isset($project)? $project->show : 0}}" {{ isset($project)?  $project->show == 1 ? 'checked': '' : ''}}>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
    {{-- <!-- /.card -->
    <div class="row box-info">
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Total des stopWords</span>
            <span class="info-box-number"> {{ $totalStopWords }}</span>
            </div>
        </div>
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-check-circle"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">StopWords ajouté aujourd'hui</span>
            <span class="info-box-number">{{ $totalToday }}</span>
            </div>
        </div>
    </div>
</div> --}}
@stop
@section('script')
@stop
