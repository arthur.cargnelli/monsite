@extends('admin.baseAdminTemplate')

@section('title', 'Project')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
<div class="home-content">

    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Les projets</h3>
        <div class="card-tools">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <a href="{{route('newProject')}}"><i class="far fa-plus-square"></i></a>
        </div>
        <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="table" style="width:100%;">
                <thead>
                    <tr>
                        <th> Id </th>
                        <th> Name </th>
                        <th> Description </th>
                        <th> Img </th>
                        <th> Url </th>
                        <th> Competences </th>
                        <th> Afficher </th>
                        <th> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($projects as  $project)
                        <tr>
                            <td> {{ $project->id }} </td>
                            <td> {{ $project->name }} </td>
                            <td class="max-line"> {{ $project->description }} </td>
                            <td> {{ $project->img }} </td>
                            <td> <a href="{{ $project->url }}" target="_blank" rel="noopener noreferrer">{{ $project->url }}</a></td>
                            <td> {{ $project->competences_count() }} </td>
                            <td>
                                <input  type="checkbox" name="show" value="{{$project->show}}" onclick="submit({{$project->id}})" {{ $project->show == 1 ? 'checked': ''}}>
                             </td>
                            <td class="actions">
                                <a href="{{route('editProject',$project->id)}}"><i class="fas fa-edit"></i></a>
                                <form action="/admin/projects/delete/{{$project->id}}" method="POST">
                                    <button type="submit" class="delete" ><i class="fas fa-trash-alt" value="Delete"></i></button>
                                    @csrf
                                    {{method_field('DELETE')}}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

</div>
@stop
@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#table').DataTable({
            "order": [[0,"desc"]],
            "language": {
                "lengthMenu": "Montrer _MENU_ éléments par page",
                "zeroRecords": "Aucun éléments",
                "info": "_PAGE_ sur _PAGES_",
                "infoEmpty": "Aucune données",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<",
                    "next": ">",
                    }
            },
            "pagingType": "full_numbers",
        });
    } );
    function submit(id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                {{ Request::session()->flash('Succes ! ', 'success') }}
            }
        };
        xhttp.open("PUT", " /admin/projects/show/ " + id, true);
        xhttp.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        xhttp.send();
    }
</script>
@stop
