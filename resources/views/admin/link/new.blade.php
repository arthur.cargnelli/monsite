@extends('admin.baseAdminTemplate')

@section('title', isset($link)? 'Edit':"New")

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nouvel Link</h3>
    <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form action="{{ isset($link)? '/admin/link/update/'.$link->id: '/admin/link/create/' }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Name : </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="name" class="form-control" value="{{isset($link)? $link->name:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="presentation_image"> Link :</label>
                </div>
                <div class="col-md-12">
                    <input type="text" name="link" class="form-control" placeholder="Link" value="{{isset($link)? $link->link:''}}" />
                </div>
            </div>
            <div class="row logo_admin">
                <div class="col-md-12">
                    <label for="files">Image : </label>
                </div>
                <div class="{{isset($link)? 'col-md-6': 'col-md-12'}}">
                    <input type="file" name="file" class="form-control"/>
                </div>
                @if(isset($link))
                    <div class="col-md-6">
                        <img src="{{$link->logo}}" alt="{{$link->name}}">
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </div>
        </form>
    </div>
</div>
    {{-- <!-- /.card -->
    <div class="row box-info">
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Total des stopWords</span>
            <span class="info-box-number"> {{ $totalStopWords }}</span>
            </div>
        </div>
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-check-circle"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">StopWords ajouté aujourd'hui</span>
            <span class="info-box-number">{{ $totalToday }}</span>
            </div>
        </div>
    </div>
</div> --}}
@stop
@section('script')
@stop
