@extends('admin.baseAdminTemplate')

@section('title', 'Home')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')

<!-- /.card -->
<div class="row box-info">
    <div class="info-box bg-info-box col-sm-12 col-md-5">
        <span class="info-box-icon"><i class="fas fa-folder indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Albums</span>
        <span class="info-box-number"> {{ $albums ?? '' }}</span>
        </div>
    </div>
    <div class="info-box bg-info-box col-sm-12 col-md-5">
        <span class="info-box-icon"><i class="fas fa-database indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Photos</span>
        <span class="info-box-number">{{ $photos ?? '' }}</span>
        </div>
    </div>
</div>
<div class="row box-info">
    <div class="info-box bg-info-box col-sm-12 col-md-5">
        <span class="info-box-icon"><i class="fas fa-project-diagram indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Projects</span>
        <span class="info-box-number"> {{ $projects ?? '' }}</span>
        </div>
    </div>
    <div class="info-box bg-info-box col-sm-12 col-md-5">
        <span class="info-box-icon"><i class="fas fa-book indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Competences</span>
        <span class="info-box-number">{{ $competences ?? '' }}</span>
        </div>
    </div>
</div>
<div class="row box-info">
    <div class="info-box bg-info-box col-sm-12 col-md-5">
        <span class="info-box-icon"><i class="fas fa-fingerprint indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Intéraction Par IP</span>
        <span class="info-box-number"> {{ $Ips ?? '' }}</span>
        </div>
    </div>
    <div class="info-box bg-info-box col-sm-12 col-md-5">
        <span class="info-box-icon"><i class="fas fa-bars indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Logs</span>
        <span class="info-box-number">{{ $logs ?? '' }}</span>
        </div>
    </div>
</div>

<div class="row box-info">
    <div class="info-box bg-info-box col-sm-12 col-md-3">
        <span class="info-box-icon"><i class="fas fa-fingerprint indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Films</span>
        <span id="nbMovies" class="info-box-number"></span>
        </div>
    </div>
    <div class="info-box bg-info-box col-sm-12 col-md-3">
        <span class="info-box-icon"><i class="fas fa-fingerprint indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Series</span>
        <span id="nbSeries" class="info-box-number"></span>
        </div>
    </div>
    <div class="info-box bg-info-box col-sm-12 col-md-3">
        <span class="info-box-icon"><i class="fas fa-fingerprint indigo"></i></span>
        <div class="info-box-content">
        <span class="info-box-text">Total Animes</span>
        <span id="nbAnimes" class="info-box-number"></span>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Twitter</h3>
        <div class="card-tools">
        </div>
    <!-- /.card-tools -->
    </div>

    <!-- /.card-header -->
    <div class="card-body">
        <a class="twitter-timeline" data-width="400" data-height="500" data-theme="dark" href="https://twitter.com/ArthurKagy?ref_src=twsrc%5Etfw">Tweets by ArthurKagy</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
    <!-- /.card-body -->
</div>
@stop
@section('script')
<script>
    function getUsers() {
      // ancien code de compatibilité, aujourd’hui inutile
    httpRequest = new XMLHttpRequest();


    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance de XMLHTTP');
      return false;
    }

    httpRequest.onreadystatechange = function() {
      if (httpRequest.status === 200) {
        var response = JSON.parse(httpRequest.responseText);

      } else {
          // il y a eu un problème avec la requête,
          // par exemple la réponse peut être un code 404 (Non trouvée)
          // ou 500 (Erreur interne au serveur)
      }
    };

    httpRequest.open('GET', 'https://jellyfin.arthurcargnelli.eu/users?apiKey=e5681ff90a8c4603994b4ed7758f2036', true);
    httpRequest.send();
  }

  function getMovies() {
      // ancien code de compatibilité, aujourd’hui inutile
    httpRequest = new XMLHttpRequest();


    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance de XMLHTTP');
      return false;
    }

    httpRequest.onreadystatechange = function() {
      if (httpRequest.status === 200) {
        var response = JSON.parse(httpRequest.responseText);

        document.getElementById('nbMovies').textContent = response['TotalRecordCount'];
        getAnimes();
      } else {
          // il y a eu un problème avec la requête,
          // par exemple la réponse peut être un code 404 (Non trouvée)
          // ou 500 (Erreur interne au serveur)
      }
    };

    httpRequest.open('GET', 'https://jellyfin.arthurcargnelli.eu/Users/de885e6ebc124183a5788c58e149ada8/Items?apiKey=ce70811e5fb74ba0868d5c5698182979&SortBy=SortName%2CProductionYear&SortOrder=Ascending&IncludeItemTypes=Movie&Recursive=true&Fields=PrimaryImageAspectRatio%2CMediaSourceCount%2CBasicSyncInfo&ImageTypeLimit=1&EnableImageTypes=Primary%2CBackdrop%2CBanner%2CThumb&StartIndex=0&ParentId=db4c1708cbb5dd1676284a40f2950aba&Limit=100', true);
    httpRequest.send();
  }

  function getSeries() {
      // ancien code de compatibilité, aujourd’hui inutile
    httpRequest = new XMLHttpRequest();


    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance de XMLHTTP');
      return false;
    }

    httpRequest.onreadystatechange = function() {
      if (httpRequest.status === 200) {
        var response = JSON.parse(httpRequest.responseText);

        document.getElementById('nbSeries').textContent = response['TotalRecordCount'];
        // getUsers()
      } else {
          // il y a eu un problème avec la requête,
          // par exemple la réponse peut être un code 404 (Non trouvée)
          // ou 500 (Erreur interne au serveur)
      }
    };

    httpRequest.open('GET', 'https://jellyfin.arthurcargnelli.eu/Users/5e3b298847c74ce187f126531ce03357/Items?apiKey=ce70811e5fb74ba0868d5c5698182979&IncludeItemTypes=Series&Recursive=true&Limit=10&ParentId=d565273fd114d77bdf349a2896867069', true);
    httpRequest.send();
  }

  function getAnimes() {
      // ancien code de compatibilité, aujourd’hui inutile
    httpRequest = new XMLHttpRequest();


    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance de XMLHTTP');
      return false;
    }

    httpRequest.onreadystatechange = function() {
      if (httpRequest.status === 200) {
        var response = JSON.parse(httpRequest.responseText);

        document.getElementById('nbAnimes').textContent = response['TotalRecordCount']
        getSeries();
      } else {
          // il y a eu un problème avec la requête,
          // par exemple la réponse peut être un code 404 (Non trouvée)
          // ou 500 (Erreur interne au serveur)
      }
    };

    httpRequest.open('GET', 'https://jellyfin.arthurcargnelli.eu/Users/de885e6ebc124183a5788c58e149ada8/Items?apiKey=ce70811e5fb74ba0868d5c5698182979&SortBy=SortName%2CProductionYear&SortOrder=Ascending&IncludeItemTypes=Series&Recursive=true&Fields=PrimaryImageAspectRatio%2CMediaSourceCount%2CBasicSyncInfo&ImageTypeLimit=1&EnableImageTypes=Primary%2CBackdrop%2CBanner%2CThumb&StartIndex=0&ParentId=0c41907140d802bb58430fed7e2cd79e&Limit=100', true);
    httpRequest.send();
  }

  document.addEventListener("DOMContentLoaded", function(event) {
    getMovies();
});
</script>
@stop

