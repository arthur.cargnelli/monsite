@extends('admin.baseAdminTemplate')

@section('title', 'Logs')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
<div class="home-content">

    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Les Logs</h3>
        <div class="card-tools">
        </div>
        <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="table" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th> IP </th>
                        <th> Browser </th>
                        <th> Os </th>
                        <th> Go to page </th>
                        <th> Location </th>
                        <th> Timezone </th>
                        <th> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($logs as $log)
                        <tr>
                            <td> {{ $log->id }} </td>
                            <td> {{ $log->ip }} </td>
                            <td> {{ $log->browser }} </td>
                            <td> {{ $log->os }} </td>
                            <td> 
                                <a href="{{ $log->page }}">{{ $log->page }}</a>
                            </td>
                            <td> {{ $log->city }}, {{$log->region}}, {{$log->country}} </td>
                            <td> {{ $log->time_zone }} </td>
                            <td class="actions">
                                <form action="/admin/logs/delete/{{$log->id}}" method="POST">
                                    <button type="submit" class="delete" ><i class="fas fa-trash-alt" value="Delete"></i></button>
                                    @csrf
                                    {{method_field('DELETE')}}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
</div>
<div class='row'>
    <div class="home-content col-sm-12 col-md-6 col-lg-6">

        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Intéractions Par Ip</h3>
            <div class="card-tools">
            </div>
            <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <canvas id="chartInteractionIp"></canvas>
            </div>
            <!-- /.card-body -->
        </div>
    </div>

    <div class="home-content col-sm-12 col-md-6 col-lg-6">

        <div class="card">
            <div class="card-header">
            <h3 class="card-title">OS</h3>
            <div class="card-tools">
            </div>
            <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <canvas id="chartBrowserInfo"></canvas>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@stop
@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#table').DataTable({
            "order": [[0,"desc"]],
            "language": {
                "lengthMenu": "Montrer _MENU_ éléments par page",
                "zeroRecords": "Aucun éléments",
                "info": "_PAGE_ sur _PAGES_",
                "infoEmpty": "Aucune données",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<",
                    "next": ">",
                    }
            },
            "pagingType": "full_numbers",
        });
    } );
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
    var ctx = document.getElementById('chartInteractionIp').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: {!!json_encode($chart->labels)!!},
            datasets: [{
                label: 'Intéractions par IP',
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: {!!json_encode($chart->dataset)!!}
            }]
        },

        // Configuration options go here
        options: {}
    });
    var ctx = document.getElementById('chartBrowserInfo').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'doughnut',

        // The data for our dataset
        data: {
            labels: {!!json_encode($chartBrowser->labels)!!},
            datasets: [{
                label: 'Navigateur',
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                data: {!!json_encode($chartBrowser->dataset)!!}
            }]
        },

        // Configuration options go here
        options: {}
    });
</script>
@stop
