@extends('admin.baseAdminTemplate')

@section('title', 'Nouvel Utilisateur')

@section('css')
    <user rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <user rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nouvel Utilisateur</h3>
    <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form action="{{'/admin/users/update/'.$user->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Name : </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="name" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="presentation_image"> Email :</label>
                </div>
                <div class="col-md-12">
                    <input type="text" name="email" class="form-control" placeholder="Email" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="password">Mot de passe : </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="password" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="passwordConfirmation"> Confirmation du mot de passe :</label>
                </div>
                <div class="col-md-12">
                    <input type="text" name="passwordConfirmation" class="form-control" placeholder="Mot de passe" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Valider</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('script')
@stop
