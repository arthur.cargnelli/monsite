@extends('admin.baseAdminTemplate')

@section('title', 'Users')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')

<div class="card">
    <div class="card-header">
    <h3 class="card-title">Les utilisateurs</h3>
    <div class="card-tools">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <a href="{{route('newUser')}}"><i class="far fa-plus-square"></i></a>
    </div>
    <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="table" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th> Name </th>
                    <th> Email </th>
                    <th> Actions </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td> {{ $user->name }} </td>
                        <td> {{ $user->email }} </td>
                        <td class="actions">
                            <a href="{{route('AdminUserProfile',$user->id)}}"><i class="fas fa-edit"></i></a>
                            <!-- <form action="{{ env('APP_URL') }}admin/users/delete/{{$user->id}}" method="POST">
                                <button type="submit" class="delete" ><i class="fas fa-trash-alt" value="Delete"></i></button>
                                @csrf
                                {{method_field('DELETE')}}
                            </form> -->
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop
@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#table').DataTable({
            "order": [[0,"desc"]],
            "language": {
                "lengthMenu": "Montrer _MENU_ éléments par page",
                "zeroRecords": "Aucun éléments",
                "info": "_PAGE_ sur _PAGES_",
                "infoEmpty": "Aucune données",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<",
                    "next": ">",
                    }
            },
            "pagingType": "full_numbers",
        });
    } );
</script>
@stop
