<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title> Admin | @yield('title')</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ env('APP_URL') }}/vendor/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ env('APP_URL') }}/vendor/adminlte/dist/css/adminlte.min.css">
  @yield('css')
  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini loading">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/logout" ><i class="fas fa-power-off"> {{ Auth::user()->name }}</i></a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/admin" class="brand-link">
      {{-- <img src="https://trizzy.io/img/logo_trizzy_white.png" alt="AdminLTE Logo" class="brand-image"
           style="opacity: .8"> --}}
      <span class="brand-text font-weight-light">Monsite &nbsp;</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/admin" class="nav-link {{ request()->is('admin') ? 'active' : '' }}">
                <i class="fas fa-home"></i>
                <p>&nbsp; Accueil</p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="{{route('AdminUsers')}}" class="nav-link {{ request()->is('admin/users*') ? 'active' : '' }}">
                <i class="fas fa-images"></i>
              <p>&nbsp; Utilisateur</p>
            </a>
        </li>
          <li class="nav-item ">
            <a href="{{route('AdminAlbum')}}" class="nav-link {{ request()->is('admin/albums*') ? 'active' : '' }}">
                <i class="fas fa-images"></i>
              <p>&nbsp; Albums</p>
            </a>
        </li>
            <li class="nav-item">
                <a href="{{route('AdminPhoto')}}" class="nav-link {{ (request()->is('admin/photos*')) ? 'active' : '' }}">
                    <i class="fas fa-image"></i>
                    <p>&nbsp; Photos</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('AdminExperience')}}" class="nav-link {{ (request()->is('admin/experiences*')) ? 'active' : '' }}">
                    <i class="fas fa-briefcase"></i>
                    <p>&nbsp; Expériences</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('AdminFormation')}}" class="nav-link {{ (request()->is('admin/formations*')) ? 'active' : '' }}">
                    <i class="fas fa-school"></i>
                    <p>&nbsp; Formations</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('AdminLink')}}" class="nav-link {{ (request()->is('admin/links*')) ? 'active' : '' }}">
                    <i class="fas fa-link"></i>
                    <p>&nbsp; Liens</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('AdminProject')}}" class="nav-link {{ (request()->is('admin/projects*')) ? 'active' : '' }}">
                    <i class="fas fa-code"></i>
                    <p>&nbsp; Projets</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('AdminCompetence')}}" class="nav-link {{ (request()->is('admin/competences*')) ? 'active' : '' }}">
                    <i class="fas fa-terminal"></i>
                    <p>&nbsp; Compétences</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('AdminContact')}}" class="nav-link {{ (request()->is('admin/contacts*')) ? 'active' : '' }}">
                    <i class="far fa-comments"></i>
                    <p>&nbsp; Messages</p>
                </a>
            </li>
            <li class="nav-item">
              <a href="{{route('AdminNews')}}" class="nav-link {{ (request()->is('admin/news')) ? 'active' : '' }}">
                <i class="far fa-newspaper"></i>
                  <p>&nbsp; News</p>
              </a>
          </li>
          <li class="nav-item">
            <a href="{{route('AdminNewsCategory')}}" class="nav-link {{ (request()->is('admin/newsCategory*')) ? 'active' : '' }}">
              <i class="far fa-folder-open"></i>
                <p>&nbsp; Catégories news</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('AdminLogsCategory')}}" class="nav-link {{ (request()->is('admin/logs*')) ? 'active' : '' }}">
              <i class="far fa-folder-open"></i>
                <p>&nbsp; Logs</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('layouts.flash-msg')

    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    {{-- <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div> --}}
    <!-- Default to the left -->
    <strong>Copyright &copy;
        {{-- {# Display current date #} --}}
        {{ date('Y') }}
      &nbsp;| All rights reserved |   <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a></strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<div id="loading" class="loader"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ env('APP_URL') }}/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ env('APP_URL') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ env('APP_URL') }}/vendor/adminlte/dist/js/adminlte.min.js"></script>
<script>
  function onReady(callback) {
    var intervalId = window.setInterval(function() {
      if (document.getElementsByTagName('body')[0] !== undefined) {
        window.clearInterval(intervalId);
        callback.call(this);
      }
    }, 1000);
  }

  function setVisible(selector, visible) {
    var display = visible ? 'block' : 'none';
    $(selector).css('display', display);
  }

  onReady(function() {
    setVisible('.wrapper', true);
    $('body').removeClass('loading');
    setVisible('#loading', false);
  });

</script>
@yield('script')
</body>
</html>
