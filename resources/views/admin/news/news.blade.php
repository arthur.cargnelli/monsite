@extends('admin.baseAdminTemplate')

@section('title', 'Actualités')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
<div class="home-content">

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Les Actualités</h3>
            <div class="card-tools">
            </div>
        <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">
            <table id="table" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th> Titre </th>
                        <th> Description </th>
                        <th> Date </th>
                        <th> Url </th>
                        <th> Catégorie </th>
                        <th> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($news as $actu)
                            <tr>
                            <td> {{$actu->id}}</td>
                            <td> {{$actu->title}} </td>
                            <td> {{$actu->description}} </td>
                            <td> {{$actu->date}} </td>
                            <td><a href="{{$actu->url}}" target="_blank">Show</a></td>
                            <td>{{$actu->newsCategory->name}}</td>
                            <td class="actions">
                                <form action="/admin/news/delete/{{$actu->id}}" method="POST">
                                    <button type="submit" class="delete" ><i class="fas fa-trash-alt" value="Delete"></i></button>
                                    @csrf
                                    {{method_field('DELETE')}}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    {{-- <!-- /.card -->
    <div class="row box-info">
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Total des stopWords</span>
            <span class="info-box-number"> {{ $totalStopWords }}</span>
            </div>
        </div>
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-check-circle"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">StopWords ajouté aujourd'hui</span>
            <span class="info-box-number">{{ $totalToday }}</span>
            </div>
        </div>
    </div>
</div> --}}
</div>
@stop
@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#table').DataTable({
            "order": [[0,"desc"]],
            "language": {
                "lengthMenu": "Montrer _MENU_ éléments par page",
                "zeroRecords": "Aucun éléments",
                "info": "_PAGE_ sur _PAGES_",
                "infoEmpty": "Aucune données",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<",
                    "next": ">",
                    }
            },
            "pagingType": "full_numbers",
        });
    } );

    function submit(id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                {{ Request::session()->flash('Succes ! ', 'success') }}
            }
        };
        xhttp.open("PUT", "{{ env('APP_URL') }}" + 'admin/albums/show/' + id, true);
        xhttp.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        xhttp.send();
    }
</script>
@stop
