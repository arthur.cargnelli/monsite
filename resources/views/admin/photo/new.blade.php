@extends('admin.baseAdminTemplate')

@section('title', isset($photo)? 'Edit':"New")

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nouvel Photo</h3>
    <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form action="{{ isset($photo)? '/admin/photos/update/'.$photo->id : '/admin/photos/create' }}" method="POST" enctype="multipart/form-data">
            @csrf

            @if(isset($photo))
                <div class="row">
                    <div class="col-md-12 show_img_admin">
                        <img src="/{{$photo->path}}" alt="{{$photo->title}}"/>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <label for="album"> Album :</label>
                </div>
                <div class="col-md-12">
                    <select id="album" name="album_id" class="form-control">
                        <option value="" selected disabled hidden>Choisir un album</option>
                        @foreach ($albums as $album)
                            @if(isset($photo) && $photo->title == $album->name)
                                <option value="{{$album->id}}" selected>{{ $album->name }}</option>
                            @else
                                <option value="{{$album->id}}">{{ $album->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="files">Image : </label>
                </div>
                <div class="col-md-12">
                    <input type="file" name="file" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
    {{-- <!-- /.card -->
    <div class="row box-info">
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Total des stopWords</span>
            <span class="info-box-number"> {{ $totalStopWords }}</span>
            </div>
        </div>
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-check-circle"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">StopWords ajouté aujourd'hui</span>
            <span class="info-box-number">{{ $totalToday }}</span>
            </div>
        </div>
    </div>
</div> --}}
@stop
@section('script')
@stop
