@extends('admin.baseAdminTemplate')

@section('title', isset($category)? 'Edit':"New")

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nouvel Catégory</h3>
    <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form action="/admin/news/categories/add" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Name Français: </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="name" class="form-control" value="{{isset($link)? $category->name:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="name_en">Name  Anglais : </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="name_en" class="form-control" value="{{isset($link)? $category->name_en:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Valider</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('script')
@stop
