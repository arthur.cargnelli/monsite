@extends('admin.baseAdminTemplate')

@section('title', 'Catégories des actualités')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
<div class="home-content">

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Les Catégories des actualités</h3>
            <div class="card-tools">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <a href="{{route('newCategory')}}"><i class="far fa-plus-square"></i></a>
            </div>
        <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">
            <table id="table" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th> Name </th>
                        <th> Nb news </th>
                        <th> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($newsCategory as $category)
                            <tr>
                            <td> {{$category->id}}</td>
                            <td> {{$category->name}} </td>
                            <td> {{$category->news_count()}} </td>
                            <td class="actions">
                                <form action="/admin/news/categories/delete/{{$category->id}}" method="POST">
                                    <button type="submit" class="delete" ><i class="fas fa-trash-alt" value="Delete"></i></button>
                                    @csrf
                                    {{method_field('DELETE')}}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    {{-- <!-- /.card -->
    <div class="row box-info">
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Total des stopWords</span>
            <span class="info-box-number"> {{ $totalStopWords }}</span>
            </div>
        </div>
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-check-circle"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">StopWords ajouté aujourd'hui</span>
            <span class="info-box-number">{{ $totalToday }}</span>
            </div>
        </div>
    </div>
</div> --}}
</div>
@stop
@section('script')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#table').DataTable({
            "order": [[0,"desc"]],
            "language": {
                "lengthMenu": "Montrer _MENU_ éléments par page",
                "zeroRecords": "Aucun éléments",
                "info": "_PAGE_ sur _PAGES_",
                "infoEmpty": "Aucune données",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<",
                    "next": ">",
                    }
            },
            "pagingType": "full_numbers",
        });
    } );

    function submit(id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                {{ Request::session()->flash('Succes ! ', 'success') }}
            }
        };
        xhttp.open("PUT", "{{ env('APP_URL') }}" + 'admin/albums/show/' + id, true);
        xhttp.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        xhttp.send();
    }
</script>
@stop
