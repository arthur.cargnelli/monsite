@extends('admin.baseAdminTemplate')

@section('title', isset($album)? 'Edit':"New")

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nouvel Album</h3>
    <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form action="{{ isset($album)? env('APP_URL').'/admin/albums/update/'.$album->id : env('APP_URL').'/admin/albums/create' }}" method="{{ isset($album)? 'POST':'POST'}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Name : </label>
                </div>
                <div class="col-md-12">
                <input type="text" name="name" class="form-control" value="{{isset($album)? $album->name:''}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="presentation_image"> Image de présentation :</label>
                </div>
                <div class="col-md-12">
                    <input type="file" name="presentation_image" class="form-control" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="files">Images : </label>
                </div>
                <div class="col-md-12">
                    <input type="file" name="file[]" class="form-control" multiple/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <label for="files">Afficher : </label>
                </div>
                <div class="col-md-12">
                    <input  type="checkbox" name="show" value="{{isset($album)? $album->show : 0}}" {{ isset($album)?  $album->show == 1 ? 'checked': '' : ''}}>
                </div>
            </div>
        </form>
    </div>
</div>
    {{-- <!-- /.card -->
    <div class="row box-info">
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-database"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Total des stopWords</span>
            <span class="info-box-number"> {{ $totalStopWords }}</span>
            </div>
        </div>
        <div class="info-box bg-info-box col-sm-12 col-md-5">
            <span class="info-box-icon"><i class="fas fa-check-circle"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">StopWords ajouté aujourd'hui</span>
            <span class="info-box-number">{{ $totalToday }}</span>
            </div>
        </div>
    </div>
</div> --}}
@stop
@section('script')
@stop
