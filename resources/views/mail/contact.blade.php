@extends('mail.baseMail')
@section('title', "New Message!")

@section('content')

<p>from : {{ $data['name'] }} </p>

<p>email: {{ $data['email'] }}</p>

<p>message : {{ $data['body'] }}</p>

@endsection
