@extends('mail.baseMail')
@section('title', "Password Reset")

@section('content')

 <p> You are receiving this email because we received a password reset request for your account. </p>

<button> <a href="{{$data['url']}}">Reset Password </a> </button>

<p>If you did not request a password reset, no further action is required.</p>

@endsection
