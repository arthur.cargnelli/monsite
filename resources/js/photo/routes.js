import Vue from 'vue'
import VueRouter from 'vue-router'
import AlbumComponent from './components/albums.vue'
import PhotosComponent from './components/photos.vue'

const Albums = Vue.component('photo',AlbumComponent)
const Photos = Vue.component('photos',PhotosComponent)

Vue.use(VueRouter)

const routes = [{
    path: "/",
    name: "Albums",
    component: Albums,
},
{

    path:"/photos/:album",
    name: "photos",
    component:Photos

}
]

export default new VueRouter({
    routes
  })
