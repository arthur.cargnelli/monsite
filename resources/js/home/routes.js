import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeComponent from './components/home.vue'

const home = Vue.component('home',HomeComponent)

const routes = [{
    path: "/",
    name: "home",
    component: home
}]

export default new VueRouter({
    routes
  })  