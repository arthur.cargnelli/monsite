import Vue from 'vue'
import VueRouter from 'vue-router'
import NewsComponent from './components/news.vue'

const News = Vue.component('news',NewsComponent)

Vue.use(VueRouter)

const routes = [{
    path: "/",
    name: "News",
    component: News,
}
]

export default new VueRouter({
    routes
  })
