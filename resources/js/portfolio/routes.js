import Vue from 'vue'
import VueRouter from 'vue-router'
import PortfolioComponent from './components/portfolio.vue'
import ProjetsComponent from './components/projets.vue'

const portfolio = Vue.component('portfolio',PortfolioComponent)
const projets = Vue.component('projets',ProjetsComponent)

Vue.use(VueRouter)

const routes = [{
    path: "/",
    name: "portfolio",
    component: portfolio
},
{
    path:"/projets",
    name:"projets",
    component:projets
}
]

export default new VueRouter({
    routes
  })
