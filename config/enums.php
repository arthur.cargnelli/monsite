<?php
return [
    'pages' => [
        'HOME' => "Home",
        'PORTFOLIO' => "Portfolio",
        'PROJECTS' => "Projects",
        'ALBUMS' => "Albums",
    ],

    'types' => [
        "DESCRIPTION" => "Description",
        "SERVICES" => "Services",
        "SKILLS" => "Skills"
    ]
];
?>
