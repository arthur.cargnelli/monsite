const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/bootstrap.js', 'public/js')
    .js('resources/js/home/home.js', 'public/js')
    .js('resources/js/photo/photos.js', 'public/js')
    .js('resources/js/portfolio/portfolio.js', 'public/js')
    .js('resources/js/news/news.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
