<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('links')->insert([
            'name'=>'CV',
            'link'=>'CV/CV.pdf'
        ]);
        DB::table('links')->insert([
            'name'=>'Malt',
            'link'=>'https://www.malt.fr/profile/arthurcargnelli'
        ]);
        DB::table('links')->insert([
            'name'=>'GitLab',
            'link'=>'https://gitlab.com/arthur.cargnelli'
        ]);
        DB::table('links')->insert([
            'name'=>'Instagram',
            'link'=>'https://www.instagram.com/tuthur9/',
            'logo'=>'/img/SVG/instagram.svg'
        ]);
        DB::table('links')->insert([
            'name'=>'Facebook',
            'link'=>'https://www.facebook.com/arthur.cargnellikagy',
            'logo'=>'/img/SVG/facebook.svg'
        ]);
        DB::table('links')->insert([
            'name'=>'Linkedin',
            'link'=>'https://www.linkedin.com/in/arthurcargnellikagy/',
            'logo'=>'/img/SVG/linkedin.svg'
        ]);
        DB::table('links')->insert([
            'name'=>'Twitter',
            'link'=>'https://twitter.com/ArthurKagy',
            'logo'=>'/img/SVG/twitter.svg'
        ]);
    }
}
