<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FormationSeeder::class);
        $this->call(ExperienceSeeder::class);
        $this->call(LinkSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TextSeeder::class);
    }
}
