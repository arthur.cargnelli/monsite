<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = config('enums.types');
        $pages = config('enums.pages');

        DB::table('texts')->insert([
            'name'=>'presentation',
            'type'=> $types["DESCRIPTION"],
            'page'=>$pages["HOME"],
            'body'=>"Originaire de Nouvelle-Calédonie, j'ai fait des études de développement web en France à Bordeaux. J'aime particulièrement voyager et la photographie. Ce sont deux 'hobbies' que mes parents m'ont transmis.",
            'default'=>true,
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);
    }
}
