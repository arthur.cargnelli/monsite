<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('experiences')->insert([
            'location' => 'Nouméa, Nouvelle-Calédonie',
            'name' => 'Stagiaire maintenance informatique',
            'compagny' => 'Cabinet Avocat Anne Gras',
            'date_start'=> date("Y-m-d", strtotime('13-07-2015')),
            'date_end'=> date("Y-m-d", strtotime('13/08/2015')),
            'current'=> false,
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('experiences')->insert([
            'location' => 'Nouméa, Nouvelle-Calédonie',
            'name' => 'Stagiaire informatique',
            'compagny' => 'Mairie de Nouméa',
            'date_start'=>date("Y-m-d", strtotime('01/07/2016')),
            'date_end'=>date("Y-m-d", strtotime('01/08/2016')),
            'current'=> false,
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('experiences')->insert([
            'location' => 'Mérignac, France',
            'name' => 'Stagiaire développeur Back-End Ruby',
            'compagny' => '3Print',
            'date_start'=>date("Y-m-d", strtotime('11/03/2019')),
            'date_end'=>date("Y-m-d", strtotime('29/06/2019')),
            'current'=> false,
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('experiences')->insert([
            'location' => 'Mérignac, France',
            'name' => 'Développeur Back-End Ruby',
            'compagny' => '3Print',
            'date_start'=>date("Y-m-d", strtotime('08/07/2019')),
            'date_end'=>date("Y-m-d", strtotime('30/08/2019')),
            'current'=> false,
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);
        DB::table('experiences')->insert([
            'location' => 'Bordeaux & Angoulême, France',
            'name' => 'Développeur FullStack',
            'compagny' => 'Mr Bot / Trizzy',
            'date_start'=>date("Y-m-d", strtotime('03/01/2020')),
            'date_end'=>null,
            'current'=> false,
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);
    }
}
