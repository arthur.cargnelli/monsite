<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formations')->insert([
            'location' => 'Laval',
            'name' => 'Première année en préparation intégrée',
            'school' => 'ESTACA (École supérieure des techniques aéronautiques et de construction automobile)',
            'year'=>'2014-2015',
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('formations')->insert([
            'location' => 'Bordeaux',
            'name' => 'Première année de Licence MISIPC',
            'school' => 'Université de Bordeaux',
            'year'=>'2015-2016',
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('formations')->insert([
            'location' => 'Bordeaux',
            'name' => 'Deuxième année de Licence Informatique',
            'school' => 'Université de Bordeaux',
            'year'=>'2016-2018',
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('formations')->insert([
            'location' => 'Bordeaux',
            'name' => 'Licence professionnelle DAWIN',
            'school' => 'Université de Bordeaux',
            'year'=>'2018-2019',
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('formations')->insert([
            'location' => 'Bordeaux',
            'name' => 'Opération 1000 Candidats',
            'school' => 'Ynov Campus & CFA',
            'year'=>'Septembre 2019',
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);

        DB::table('formations')->insert([
            'location' => 'Bordeaux',
            'name' => 'Mastère "Expert Développement Web',
            'school' => 'Bordeaux Ynov Campus',
            'year'=>'2019',
            'current' => true,
            'created_at' => Carbon::now()->subDay()->format('Y-m-d H:i:s')
        ]);
    }
}
