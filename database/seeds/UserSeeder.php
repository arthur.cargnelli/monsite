<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Arthur Cargnelli',
            'email'=>'arthur.cargnelli@gmail.com',
            'email_verified_at'=>NULL,
            'password'=> '$2y$10$fgaCnA6EnH1TGbG2br46j.ZqchdjhCyIjY1BTYqZb7R45wc29sOre',
            'remember_token'=>NULL,
            'created_at'=>'2020-03-04 15:02:27',
            'updated_at'=>'2020-03-04 15:02:27'
        ]);
    }
}