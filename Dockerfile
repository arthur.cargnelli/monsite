# Dockerfile
FROM php:7.4-apache

ENV COMPOSER_ALLOW_SUPERUSER=1

WORKDIR /var/www/html/monsite

RUN docker-php-source extract \
	# do important things \
	&& docker-php-source delete

RUN apt-get update -qy && \
    apt-get install -y \
    git \
    libicu-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    unzip \
    zip && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && apt-get install -y build-essential && \
    apt-get install -y gnupg \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list\
    && apt-get install nodejs apt-transport-https yarn -y

# PHP Extensions
RUN docker-php-ext-install -j$(nproc) opcache pdo_mysql
ADD docker/php/php.ini /usr/local/etc/php/php.ini
ADD docker/apache2/.htaccess /var/www/html/.htaccess
RUN docker-php-ext-configure gd --with-freetype --with-jpeg


# Conf Apache2
COPY ./docker/apache2/apache2.conf /etc/apache2/conf-available/apache2.conf
RUN echo "ServerName arthurcargnelli.eu" >> /etc/apache2/conf-available/apache2.conf
RUN a2enconf apache2.conf
COPY ./docker/apache2/vhosts.conf /etc/apache2/sites-available/app.conf
RUN usermod -u 1000 www-data && chown -R www-data:www-data /var/www/html/monsite
RUN a2enmod ssl;service apache2 restart;a2enmod proxy_http;service apache2 restart;a2enmod rewrite;service apache2 restart;a2enmod headers;service apache2 restart; a2enmod expires;service apache2 restart
RUN a2dissite 000-default.conf && a2ensite app.conf && service apache2 restart;
RUN apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#COPY . /var/www/html/monsite/

#COPY storage /var/www/html/monsite/

#COPY composer.* /var/www/html/monsite/

#COPY package.json /var/www/html/monsite/

# RUN cd /monsite \
#     && composer install --prefer-dist --no-progress --no-suggest\
#     && yarn global add svgo\
#     && yarn install

EXPOSE 80
