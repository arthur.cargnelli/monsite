<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
Use App\Mail\ContactMail;

class mailer extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('logger');
    // }

    public function send(Request $request){
        $resultsJson = $this->captchaverify($request['g-recaptcha-response']);
        if(!$resultsJson){
            return back()->with('error','Your mail has not been sent , please try again later.');
        }
        else{
            $data = array('name'=>$request->name, 'body' => $request->body, 'email'=>$request->email, 'userMail' => 'arthur@arthurcargnelli.eu');
            Mail::send(new ContactMail($data));
            return back()->with('success', 'Your mail has been send');
        }
    }

    # get success response from recaptcha and return it to controller
    function captchaverify($recaptcha){
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "secret"=>"6Leh098UAAAAALcd7-4wPgMWQ787A1s4TgopZTW4","response"=>$recaptcha));
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);

    
    return $data->success;        
}
    
}
