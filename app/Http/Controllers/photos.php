<?php

namespace App\Http\Controllers;

use App\Album;

class photos extends Controller
{

    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('logger');
    // }

    public function getPhotos($id){
        $photos = Album::find($id)->images;
        return json_encode($photos);
    }

}
