<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\newsCategory;
use Illuminate\Http\Request;

class newsCategoriesAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }
    public function delete($id){
        $name = newsCategory::find($id)->name;
        newsCategory::find($id)->delete();
        return redirect()->route('AdminNewsCategory')
            ->with('success','You have successfully delete '.$name);
    }

    public function new()
    {
        return view('admin.newsCategory.new');
    }

    public function edit($id)
    {
        $category = NewsCategory::find($id);
        return view('admin.newsCategory.new',compact('category'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'name_en' => 'required|max:255',
        ]);
        $category = new NewsCategory;
        $category->name = strtoupper($request->name);
        $category->name_en = strtoupper($request->name_en);
        $category->save();
        return redirect()->route('AdminNewsCategory')
            ->with('success','You have successfully create '.$category->name.'.');

    }


}
