<?php

namespace App\Http\Controllers\admin;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\project;
use App\competence;

class projectAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }

    public function new()
    {
        $competences = Competence::all(['id', 'name']);
        return view('admin.project.new', compact('competences'));
    }

    public function edit($id)
    {
        $project = Project::find($id);
        $competences = Competence::all(['id', 'name']);
        return view('admin.project.new',compact('project', 'competences'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'file' => 'required|mimes:jpeg,jpg,png,svg',
            'url' => 'nullable|max:255',
            'competences' => 'array'
        ]);

        $project = new project;
        $project->name = $request->get('name');
        $project->description = $request->get('description');
        $project->url = $request->get('url');
        if($request->hasfile('file')) {
            $fileName = time().rand(0, 1000).pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('file')->getClientOriginalExtension();
            $request->file('file')->move(public_path('Uploads/projects/'.$project->name),$fileName);
            $input['file'] = $fileName;
            $project->img = 'Uploads/projects/'.$project->name.'/'.$fileName;
        }
        $project->save();
        if(isset($request->competences) && count($request->competences) != 0){
            $competences = Competence::find($request->competences);
            foreach($competences as $competence){
                $competence->projects()->attach($project);
            }
        }
        return redirect()->route('AdminProject')
            ->with('success','You have successfully create '.$project->name.'.');

    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'file' => 'required|mimes:jpeg,jpg,png,svg',
            'url' => 'nullable|max:255',
            'competences' => 'array'
        ]);
        $project = Project::find($id);
        $project->name = $request->get('name');
        $project->description = $request->get('description');
        $project->url = $request->get('url');
        if($request->hasfile('file')) {
            $fileName = time().rand(0, 1000).pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('file')->getClientOriginalExtension();
            $request->file('file')->move(public_path('Uploads/projects/'.$project->name),$fileName);
            $input['file'] = $fileName;
            $project->img = 'Uploads/projects/'.$project->name.'/'.$fileName;
        }
        if(count($request->competences) != 0){
            $competences = Competence::find($request->competences);
            $project->competences()->attach($competences);
        }
        $project->save();
        return redirect()->route('AdminProject')
            ->with('success','You have successfully update '.$project->name.'.');

    }

    public function delete(Request $request,$id){
        $name = Project::find($id)->name;
        Project::find($id)->delete();
        return redirect()->route('AdminProject')
            ->with('success','You have successfully update '.$name);
    }

    public function editShow(Request $request,$id){
        $project = Project::find($id);
        $project->show = $project->show == 1 ? 0 : 1;
        $project->save();
        return response()->json(['success' => 'yay']);
    }
}
