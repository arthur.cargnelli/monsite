<?php

namespace app\Http\Controllers\admin;

use app\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Album;
use App\photo;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer as FacadesImageOptimizer;

class albumAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }

    public function new()
    {
        return view('admin.album.new');
    }

    public function edit($id)
    {
        $album = Album::find($id);
        return view('admin.album.new',compact('album'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'presentation_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'files.*' => 'mimes:doc,pdf,docx,txt,zip,jpeg,jpg,png',
        ]);
        $presentation_image = new Photo;
        $album = new Album;
        $album->name = $request->get('name');
        $album->save();
        if($request->hasfile('presentation_image')) {
            $fileName = time().rand(0, 1000).pathinfo($request->file('presentation_image')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('presentation_image')->getClientOriginalExtension();
            $request->file('presentation_image')->move(public_path('Uploads/'.$album->id),$fileName);
            $input['presentation_image'] = $fileName;
            FacadesImageOptimizer::optimize(public_path('Uploads/'.$album->id.'/'.$fileName));
            convert(public_path('Uploads/'.$album->id.'/'.$fileName),public_path('Uploads/'.$album->id.'/'.$fileName));
            $presentation_image->title = $request->get('name');
            $presentation_image->path = 'Uploads/'.$album->id.'/'.$fileName;
            $album->presentation_image = $presentation_image->path;
            $presentation_image->album_id = $album->id;
            $presentation_image->save();
        }
        if($request->hasfile('file')) {
            foreach($request->file('file') as $file)
            {
                $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('Uploads/'.$album->id.'/'),$fileName);
                $input['files'] = $fileName;
                $photo = new Photo;
                $photo->title = $album->name;
                FacadesImageOptimizer::optimize(public_path('Uploads/'.$album->id.'/'.$fileName));
                convert(public_path('Uploads/'.$album->id.'/'.$fileName),public_path('Uploads/'.$album->id.'/'.$fileName));
                $photo->path = 'Uploads/'.$album->id.'/'.$fileName;
                $photo->album_id = $album->id ;
                $photo->save();
            }
        }
        $album->save();
        return redirect()->route('AdminAlbum')
            ->with('success','You have successfully create '.$album->name.'.');

    }

    public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|max:255',
            'presentation_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'file.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $presentation_image = new Photo;
        $album = Album::find($id);
        $album->name = $request->get('name');
        if($request->hasfile('presentation_image')){
            $fileName = time().rand(0, 1000).pathinfo($request->file('presentation_image')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('presentation_image')->getClientOriginalExtension();
            $request->file('presentation_image')->move(public_path('Uploads/'.$album->id.'/'),$fileName);
            $input['presentation_image'] = $fileName;
            FacadesImageOptimizer::optimize(public_path('Uploads/'.$album->id.'/'.$fileName));
            convert(public_path('Uploads/'.$album->id.'/'.$fileName),public_path('Uploads/'.$album->id.'/'.$fileName));
            $presentation_image->title = $request->get('name');
            $presentation_image->path = public_path('Uploads/'.$album->id.'/'.$fileName);
            $album->presentation_image = public_path('Uploads/'.$album->id.'/'.$fileName);
            $presentation_image->album_id = $album->id;
            $presentation_image->save();
        }
        if($request->hasfile('file')) {
            foreach($request->file('file') as $file)
            {
                $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('Uploads/'.$album->id.'/'),$fileName);
                $input['files'] = $fileName;
                $photo = new Photo;
                $photo->title = $album->name;
                FacadesImageOptimizer::optimize(public_path('Uploads/'.$album->id.'/'.$fileName));
                convert(public_path('Uploads/'.$album->id.'/'.$fileName),public_path('Uploads/'.$album->id.'/'.$fileName));
                $photo->path = 'Uploads/'.$album->id.'/'.$fileName;
                $photo->album_id = $album->id ;
                $photo->save();
            }
        }
        $album->save();
        return redirect()->route('AdminAlbum')
            ->with('success','You have successfully update '.$album->name);
    }

    public function delete(Request $request,$id){
        $name = Album::find($id)->name;
        Album::find($id)->delete();
        return redirect()->route('AdminAlbum')
            ->with('success','You have successfully delete '.$name);
    }

    public function editShow(Request $request,$id){
        $project = Album::find($id);
        $project->show = $project->show == 1 ? 0 : 1;
        $project->save();
        return response()->json(['success' => 'yay']);
    }

}
