<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\news;

class newsAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }

   public function fetchNews(){
        $categories = newsCategory::All();
        foreach($categories as $category) {
            $url = 'http://newsapi.org/v2/everything?'.'q='.$category.'&'.'from=2021-01-14&'.'sortBy=popularity&'.'apiKey=fc135f69a5db40fdb53ef02e9ec7dcb1';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                "secret"=>"6Leh098UAAAAALcd7-4wPgMWQ787A1s4TgopZTW4","response"=>$recaptcha));
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);
            var_dump($data);
            if ($data['status'] == 'ok' && $data['articles'].length != 0) {
                foreach($data['articles'] as $article) {
                    $news = new news();
                    $news->title = $request->$article['title'];
                    $news->description = $article['description'];
                    $news->date = new DateTime($article['publishedAt']);
                    $news->url = $article['url'];
                    $news->newsCategory()->associate($category);
                    $news->save();
                }
            }
        }
        return 'ok';
    }
    public function delete($id){
        $name = news::find($id)->title;
        news::find($id)->delete();
        return redirect()->route('AdminNews')
            ->with('success','You have successfully delete '.$name);
    }

}
