<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\experience;

class experienceAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }

    public function new()
    {
        return view('admin.experience.new');
    }

    public function edit($id)
    {
        $experience = Experience::find($id);
        return view('admin.experience.new',compact('experience'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'location' => 'required|max:255',
            'compagny' => 'required|max:255',
            'name' => 'required|max:255',
            'date_start' => 'required|date',
            'date_ent' => 'nullable|date',
        ]);
        $experience = new Experience;
        $experience->location = $request->location;
        $experience->compagny = $request->compagny;
        $experience->name = $request->name;
        $experience->current = $request->current == true ? 1:0;
        $experience->date_start = date('Y-m-d',strtotime($request->date_start));
        $experience->date_end = isset($request->date_end)? date('Y-m-d',strtotime($request->date_end)): null;
        $experience->save();
        return redirect()->route('AdminExperience')
            ->with('success','You have successfully create '.$experience->name.'.');

    }

    public function update(Request $request,$id){
        $request->validate([
            'location' => 'required|max:255',
            'compagny' => 'required|max:255',
            'name' => 'required|max:255',

            'date_start' => 'required|date',
            'date_ent' => 'nullable|date',
        ]);
        $experience = Experience::find($id);
        $experience->location = $request->location;
        $experience->compagny = $request->compagny;
        $experience->name = $request->name;
        $experience->current = $request->current == true ? 1:0;
        $experience->date_start = date('Y-m-d',strtotime($request->date_start));
        $experience->date_end = isset($request->date_end)? date('Y-m-d',strtotime($request->date_end)): null;
        $experience->save();
        return redirect()->route('AdminExperience')
            ->with('success','You have successfully update '.$experience->name);
    }

    public function delete(Request $request,$id){
        $name = Experience::find($id)->name;
        Experience::find($id)->delete();
        return redirect()->route('AdminExperience')
            ->with('success','You have successfully delete '.$name);
    }
}
