<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Album;
use App\photo;

class photoAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }

    public function new()
    {
        $albums = Album::all(['id', 'name']);
        return view('admin.photo.new',compact('albums'));
    }

    public function edit($id)
    {
        $photo = Photo::find($id);
        $albums = Album::all(['id', 'name']);
        return view('admin.photo.new',compact('albums','photo'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'title' => 'nullable|max:255',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'album_id' => 'required|numeric',
        ]);
        $album = Album::find($request->get('album_id'));
        $photo = new Photo;
        $photo->album()->associate($album);
        $filename = '';
        if($request->get('title') == null || $request->get('title') == ''){
            $photo->title = $album->name;
        }
        else{
            $photo->title = $request->get('title');
        }

        if($request->hasfile('file')) {
            $file = $request->file('file');
            $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path('Uploads/'.$album->id.'/'),$fileName);
            $input['files'] = $fileName;
            $photo->path = public_path('Uploads/'.$album->id.'/'.$fileName);
        }
        $photo->save();
        return redirect()->route('AdminAlbum')
            ->with('success','You have successfully create '.$filename.'.');

    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'title' => 'nullable|max:255',
            'file' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'album_id' => 'nullable|numeric',
        ]);

        $photo = Photo::find($id);
        if($request->get('album_id') != null){
            $album = Album::find($request->get('album_id'));
            $photo->album()->dissociate();
            $photo->album()->associate($album);
        }
        else{
            $album = $photo->album();
        }
        $photo->album()->associate($album);
        $filename = '';
        if($request->get('title') == null || $request->get('title') == ''){
            $photo->title = $album->name;
        }
        else{
            $photo->title = $request->get('title');
        }

        if($request->hasfile('file')) {
            $file = $request->file('file');
            $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path('Uploads/'.$album->id.'/'),$fileName);
            $input['files'] = $fileName;
            $photo->path = public_path('Uploads/'.$album->id.'/'.$fileName);
        }
        $photo->save();
        return redirect()->route('AdminPhoto')
            ->with('success','You have successfully update '.$filename.'.');

    }

    public function delete(Request $request,$id){
        $id = Photo::find($id)->id;
        Photo::find($id)->delete();
        return redirect()->route('AdminPhoto')
            ->with('success','You have successfully delete photo id: '.$id);
    }
}
