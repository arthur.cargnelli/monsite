<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\formation;

class formationAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
        $this->middleware('auth');
        $this->middleware('web');
   }

    public function new()
    {
        return view('admin.formation.new');
    }

    public function edit($id)
    {
        $formation = Formation::find($id);
        return view('admin.formation.new',compact('formation'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'location' => 'required|max:255',
            'school' => 'required|max:255',
            'year' => 'required|max:255',
        ]);
        $formation = new Formation;
        $formation->name = $request->name;
        $formation->location = $request->location;
        $formation->school = $request->school;
        $formation->year = $request->year;
        $formation->current = $request->current == true? 1 : 0;
        $formation->save();
        return redirect()->route('AdminFormation')
            ->with('success','You have successfully create '.$formation->name.'.');
    }

    public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|max:255',
            'location' => 'required|max:255',
            'school' => 'required|max:255',
            'year' => 'required|max:255',
        ]);
        $formation = Formation::find($id);
        $formation->name = $request->name;
        $formation->location = $request->location;
        $formation->school = $request->school;
        $formation->year = $request->year;
        $formation->current = $request->current == true? 1 : 0;
        $formation->save();
        return redirect()->route('AdminFormation')
            ->with('success','You have successfully update '.$formation->name.'.');
    }

    public function delete(Request $request,$id){
        $name = Formation::find($id)->name;
        Formation::find($id)->delete();
        return redirect()->route('AdminFormation')
            ->with('success','You have successfully delete '.$name);
    }
}
