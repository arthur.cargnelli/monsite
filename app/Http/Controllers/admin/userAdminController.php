<?php

namespace App\Http\Controllers\admin;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\user;
use App\competence;

class userAdminController extends Controller
{
        /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }

   public function list()
   {
       $users = user::orderBy('id')->get();
       return view('admin.users.users',compact('users'));
   }

   public function new()
   {
       $competences = Competence::all(['id', 'name']);
       return view('admin.users.new', compact('competences'));
   }

   public function edit($id)
   {
       $user = user::find($id);
       return view('admin.users.profile',compact('user'));
   }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create(Request $request)
   {
       $request->validate([
           'name' => 'required|max:255',
           'email' => 'required|max:255',
           'password' => 'required|max:255',
           'password_confirmation' => 'required|max:255',
       ]);

       $user = new user;
       $user->name = $request->get('name');
       $user->email = $request->get('description');
       if ($request->get('password') == $request->get('password_confirmation')) {
        $user->password = $request->get('url');
       }
       return redirect()->route('AdminUsers')
           ->with('success','You have successfully create '.$user->name.'.');

   }

   public function updatePassword(Request $request,$id)
   {
        $request->validate([
            'password' => 'required|max:255',
            'passwordConfirmation' => 'required|max:255',
        ]);
        // $secret = env('APP_SECRET', 'ARTHURCARGNELLISECRETSITE');
        $user = User::find($id);
        if($request->get('password') == $request->get('passwordConfirmation')) {
            $password = bcrypt($request->get('password'));
            $user->password = $password;
            $user->save();
            return redirect()->route('AdminUsers')
                ->with('success','You have successfully update '.$user->name.'.');
        }
        return redirect()->route('AdminUsers')
                ->with('error','You have not successfully update '.$user->name.'.');

   }

   public function update(Request $request,$id)
   {
       $request->validate([
           'name' => 'required|max:255',
           'email' => 'required|max:255',
       ]);
       $user = User::find($id);
       $user->name = $request->get('name');
       $user->email = $request->get('email');
       $user->save();
       return redirect()->route('AdminUsers')
           ->with('success','You have successfully update '.$user->name.'.');

   }

   public function delete(Request $request,$id){
       $name = User::find($id)->name;
       User::find($id)->delete();
       return redirect()->route('AdminUsers')
           ->with('success','You have successfully update '.$name);
   }

}