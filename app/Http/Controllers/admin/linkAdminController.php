<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\link;

class linkAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }

    public function new()
    {
        return view('admin.link.new');
    }

    public function edit($id)
    {
        $link = Link::find($id);
        return view('admin.link.new',compact('link'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'link' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $link = new Link;
        $link->name = $request->get('name');
        $link->link = $request->get('link');
        $link->save();
        if($request->hasfile('logo')) {
            $fileName = time().rand(0, 1000).pathinfo($request->file('presentation_image')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('presentation_image')->getClientOriginalExtension();
            $request->file('presentation_image')->move(public_path('Uploads/logos'),$fileName);
            $input['logo'] = $fileName;
            $link->logo = public_path('Uploads/logos/'.$fileName);
        }
        $link->save();
        return redirect()->route('AdminLink')
            ->with('success','You have successfully create '.$link->name.'.');

    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'link' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $link = Link::find($id);
        $link->name = $request->get('name');
        $link->link = $request->get('link');
        $link->save();
        if($request->hasfile('logo')) {
            $fileName = time().rand(0, 1000).pathinfo($request->file('presentation_image')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('presentation_image')->getClientOriginalExtension();
            $request->file('presentation_image')->move(public_path('Uploads/logos'),$fileName);
            $input['logo'] = $fileName;
            $link->logo = public_path('Uploads/logos/'.$fileName);
        }
        $link->save();
        return redirect()->route('AdminLink')
            ->with('success','You have successfully update '.$link->name.'.');

    }

    public function delete(Request $request,$id){
        $name = Link::find($id)->name;
        Link::find($id)->delete();
        return redirect()->route('AdminLink')
            ->with('success','You have successfully delete '.$name);
    }
}
