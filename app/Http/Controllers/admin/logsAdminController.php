<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\log;

class logsAdminController extends Controller
{
    /*
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('web');
   }
    public function delete($id){
        $name = Log::find($id)->ip;
        Log::find($id)->delete();
        return redirect()->route('AdminLogsCategory')
            ->with('success','You have successfully delete '.$name);
    }

}
