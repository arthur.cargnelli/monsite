<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\competence;
use App\project;

class competenceAdminController extends Controller
{
    // CRUD

    public function new(){
        $projects = Project::all(['id', 'name']);
        return view('admin.competence.new', compact('projects'));
    }
    public function create(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'projects' => 'nullable|array|min_array_size:0,'
        ]);
        $competence = new Competence;
        $competence->name = $request->get('name');
        if($request->hasfile('logo')) {
            $fileName = time().rand(0, 1000).pathinfo($request->file('logo')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('/img/competences/'),$fileName);
            $input['logo'] = $fileName;
            $competence->logo = ('/img/competences/'.$fileName);
        }
        if(isset($request->projects) && count($request->projects) != 0){
            $projects = Project::find($request->projects);
            $competence->projects()->attach($projects);
        }
        $competence->save();
        return redirect()->route('AdminCompetence')
            ->with('success','Une nouvelle compétence à bien été créer');
    }
    public function edit($id){
        $competence = Competence::find($id);
        $projects = Project::all(['id', 'name']);
        return view('admin.competence.new', compact('competence', 'projects'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required|max:255',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'projects' => 'nullable|array|min_array_size:0,'
        ]);
        $competence = Competence::find($id);
        $competence->name = $request->get('name');
        if($request->hasfile('logo')) {
            $fileName = time().rand(0, 1000).pathinfo($request->file('logo')->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('competences/'),$fileName);
            $input['logo'] = $fileName;
            $competence->logo = public_path('competences/'.$fileName);
        }
        if(isset($request->projects) && count($request->projects) != 0){
            $projects = Project::find($request->projects);
            $competence->projects()->attach($projects);
        }
        $competence->save();
        return redirect()->route('AdminCompetence')
            ->with('success','La compétence '.$competence->name.' à bien été modifier');
    }
    public function delete($id){
        $name = Competence::find($id)->name;
        $competence = Competence::find($id)->delete();
        return redirect()->route('AdminCompetence')
            ->with('success','La compétence'.$name.' à bien été supprimer');
    }
}
