<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\contact;

class contactAdminController extends Controller
{
    // CRUD

    public function show($id){
        $contact = Contact::find($id);
        return view('admin.contact.show', compact('contact'));
    }

    public function delete($id){
        $name = Contact::find($id)->name;
        $competence = Contact::find($id)->delete();
        return redirect()->route('AdminContact')
            ->with('success','Le message de '.$name.' à bien été supprimer');
    }
}
