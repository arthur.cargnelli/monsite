<?php

namespace App\Http\Controllers;
use App\Album;

class albums extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('logger');
    // }
    
    function show(){
        return view('albums');
    }

    public function getAlbums(){
        $albums = Album::orderBy('created_at', 'DESC')->get();
        return json_encode($albums);
    }

    public function getAlbum($id){
        $album = Album::find($id);
        return json_encode($album);
    }
}
