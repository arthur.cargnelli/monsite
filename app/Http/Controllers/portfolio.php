<?php

namespace App\Http\Controllers;

use App\formation;
use App\experience;
use App\competence;
use App\project;
use App\link;

class portfolio extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('logger');
    // }

    function show(){
        return view("portfolio");
    }

    public function getFormations(){
        $formations = Formation::all();
        return json_encode($formations);
    }
    public function getExperiences(){
        $experiences = Experience::all();
        return json_encode($experiences);
    }
    public function getCompetences(){
        $competences = Competence::all();
        return json_encode($competences);
    }
    public function getCV(){
        $CV = Link::find(1);
        return json_encode($CV);
    }
    public function getProjects(){
        $projects = Project::orderBy('created_at', 'DESC')->get();
        foreach($projects as $project){
            $project['competences'] = $project->competences()->get();
        }
        return json_encode($projects);
    }
}
