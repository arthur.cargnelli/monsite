<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\news;
use App\ResultNewsApi;
use App\newsCategory;
use DateTime;

class newsController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('logger');
    // }

    function show(){
        $lastmonth = date("Y-m-d", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
        $tomorrow = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+1,   date("Y")));
        $categories = newsCategory::All();
        foreach($categories as $category) {
            $url = 'http://newsapi.org/v2/everything?'.'q='.$category->name.'&'.'from='.$lastmonth.'&to='.$tomorrow.'&'.'sortBy=relevancy&apiKey=fc135f69a5db40fdb53ef02e9ec7dcb1&language=fr&page=1';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);
            if ( $data != null && $data->status == 'ok' && $data->totalResults != 0) {
                foreach($data->articles as $article) {
                    $test = news::Where('title', $article->title)->first();
                    if ($test == null && $article->title != null && $article->description != null) {
                        $news = new news();
                        $news->title = $article->title;
                        $news->description = $article->description;
                        $news->date = new DateTime($article->publishedAt);
                        $news->url = $article->url;
                        $news->newsCategory()->associate($category);
                        $news->save();
                    }

                }
            }

            $url = 'http://newsapi.org/v2/everything?'.'q='.$category->name_en.'&'.'from='.$lastmonth.'&to='.$tomorrow.'&'.'sortBy=relevancy&apiKey=fc135f69a5db40fdb53ef02e9ec7dcb1&language=en&page=1';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);
            if ( $data != null && $data->status == 'ok' && $data->totalResults != 0) {
                foreach($data->articles as $article) {
                    $test = news::Where('title', $article->title)->first();
                    if ($test == null && $article->title != null && $article->description != null) {
                        $news = new news();
                        $news->title = $article->title;
                        $news->description = $article->description;
                        $news->date = new DateTime($article->publishedAt);
                        $news->url = $article->url;
                        $news->newsCategory()->associate($category);
                        $news->save();
                    }

                }
            }
        }
        return view('news.google');
    }

    public function getNewsCategories(){
        $news = newsCategory::orderBy('name', 'ASC')->get();
        return json_encode($news);
    }
    
    public function getNews(Request $request){
        $query = $request->query();
        $category = $query["category"];
        $page = 0;
        if (isset($query["page"])) {
            $page = ((int)$request->query("page")) - 1;
        }
        $newsCategory = newsCategory::where('name', strtoupper ($category))->first();
        $result = new ResultNewsApi();
        if ( isset($query["category"]) && $newsCategory != "null" && $newsCategory != NULL)
        {
            $news = $newsCategory->news()->orderBy('date', 'DESC')->skip(10*$page)->take(10)->get();
            $result->data = $news;
            $result->totalCount = $newsCategory->news_count();
            return json_encode($result);
        }
        $news = news::orderBy('date', 'DESC')->skip(10*$page)->take(10)->get();
        $result->data = $news;
        $result->totalCount = news::all()->count();
        return json_encode($result);
    }

}
