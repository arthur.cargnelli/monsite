<?php

namespace App\Http\Controllers;
use App\link;
use App\Album;
use App\photo;
use App\experience;
use App\formation;
use App\project;
use App\competence;
use App\contact;
use App\newsCategory;
use App\news;
use App\log;
use App\Chart;
use Illuminate\Support\Facades\DB;


class adminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home(){
        $albums = count(Album::all());
        $photos = count(Photo::all());
        $projects = count(Project::all());
        $competences = count(Competence::all());
        $Ips = count(DB::table('logs')->select(DB::raw('ip'))
        ->groupBy('ip')->get());
        $logs = count(Log::all());
        return view('admin.home', compact('albums', 'photos', 'projects', 'competences', 'Ips', 'logs'));
    }
    public function album(){
        $albums = Album::all();
        return view('admin.album.album',compact('albums'));
    }
    public function experience(){
        $experiences = Experience::all();
        return view('admin.experience.experience',compact('experiences'));
    }
    public function formation(){
        $formations = Formation::all();
        return view('admin.formation.formation',compact('formations'));
    }
    public function link(){
        $links = Link::all();
        return view('admin.link.link', compact('links'));
    }
    public function photo(){
        $photos = Photo::all();
        return view('admin.photo.photo',compact('photos'));
    }
    public function project(){
        $projects = Project::all();
        $competences = Competence::all();
        return view('admin.project.project',compact('projects', 'competences'));
    }
    public function competence(){
        $competences = Competence::all();
        return view('admin.competence.competence',compact('competences'));
    }
    public function contact(){
        $contacts = Contact::all();
        return view('admin.contact.contact',compact('contacts'));
    }
    public function news(){
        $news = news::all();
        return view('admin.news.news',compact('news'));
    }
    public function newsCategory(){
        $newsCategory = newsCategory::all();
        return view('admin.newsCategory.newsCategory',compact('newsCategory'));
    }
    public function logs(){
        $Ips = DB::table('logs')->select(DB::raw('ip, count(ip) as nbip'))
        ->groupBy('ip')
        ->pluck('ip', 'nbip')->all();
        $Browsers = DB::table('logs')->select(DB::raw('os'))
        ->get();
        $browsersLabel = array(
            'Linux' => 0,
            'Mac' => 0,
            'Windows' => 0,
            'Andoid / Others' => 0,
            'iOS / iPadOS' => 0,
            'NAN' => 0,
        );
        foreach ($Browsers as $os) {
            if ($Browsers->count() > 0) {
                if ((str_contains (  $os->os , 'Linux'  ) || str_contains (  $os->os , 'Ubuntu'  ))) {
                    $browsersLabel['Linux'] += 1;
                }
                if (str_contains (  $os->os , 'Mac'  )) {
                    $browsersLabel['Mac'] += 1;
                }
                if (str_contains (  $os->os , 'Windows'  )) {
                    $browsersLabel['Windows'] += 1;
                }
                if (str_contains (  $os->os , 'Android'  ) || str_contains (  $os->os , 'Symbian'  ) || str_contains (  $os->os , 'AndroidOS'  )) {
                    $browsersLabel['Andoid / Others'] += 1;
                }
                if (str_contains (  $os->os , 'iOS'  ) || str_contains (  $os->os , 'iPadOs'  )) {
                    $browsersLabel['iOS / iPadOS'] += 1;
                } 
                if (!str_contains (  $os->os , 'iOS'  ) && !str_contains (  $os->os , 'iPadOs'  ) && !str_contains (  $os->os , 'Android'  ) && !str_contains (  $os->os , 'Symbian'  ) && !str_contains (  $os->os , 'AndroidOS'  )
                && !str_contains (  $os->os , 'Windows'  ) && !str_contains (  $os->os , 'Mac'  ) && !str_contains (  $os->os , 'Linux'  ) && !str_contains (  $os->os , 'Ubuntu'  )) {
                    $browsersLabel['NAN'] += 1;
                }
            }
        }
        $logs = Log::all();
        $totalLogs = $logs->count();
        if ($totalLogs > 0) {
            foreach (array_Keys($browsersLabel) as $os) {
                $browsersLabel[$os] = round(($browsersLabel[$os] / $totalLogs ) * 100);
            }
        }
        $chartBrowser = new Chart;
        $chartBrowser->labels = (array_Keys($browsersLabel));
        $chartBrowser->dataset = (array_Values($browsersLabel));
        $chart = new Chart;
        $chart->labels = (array_values($Ips));
        $chart->dataset = (array_keys($Ips));
        return view('admin.logs.logs',compact('logs', 'chart', 'chartBrowser'));
    }
}
