<?php

namespace App\Http\Middleware;
use Browser;
use App\log;
use Closure;

class Logger
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : null ;
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://freegeoip.app/json/".$ip,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "content-type: application/json"
        ),
        ));

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);
        $BROWSER_EXECPTION = ["curl 7.64", "Googlebot 2.1", "bingbot 2","SemrushBot 7", "libwww-perl 6.49", "bingbot", "Go-http-client 1.1", "AhrefsBot 7", "; bot", "MJ12bot 1.4.8", "YandexBot 3", "Nimbostratus-Bot", "FacebookBot 1.1", "DuckDuckGo-Favicons-Bot 1", "libwww-perl 5.833", "curl 7.58", "curl 7.29", "curl 7.54"];
        $IP_EXECPTION = ["127.0.1", "149.91.81.78", "176.128.141.68", "149.91.82.16"];
        $DOMAIN_EXECPTION = ["149.91.81.78", "78.81.91.149.ipv4.netrix.fr", "149.91.81.78:443", "149.91.81.78:8080", ];
        $DOMAIN = $request->url();
	$URL = $request->fullUrl();
/*        if ( $ip != null && !str_contains($URL, 'arthurcargnelli.fr') && !in_array($ip, $IP_EXECPTION) && str_starts_with($request->fullUrl(), 'https') && !in_array(Browser::browserName(), $BROWSER_EXECPTION) && !in_array(explode('/', $DOMAIN)[2], $DOMAIN_EXECPTION))
        {
            $log = new log();
            $log->ip = $ip;
            $log->browser = Browser::browserName();
            $log->os = Browser::platformName();
            $log->page =  $request->fullUrl();
            $log->city = $response->city;
            $log->region = $response->region_name;
            $log->country = $response->country_name;
            $log->latitude = $response->latitude;
            $log->longitude = $response->longitude;
            $log->time_zone = $response->time_zone;
            //$log->save();
	}*/
        return $next($request);
    }
}
