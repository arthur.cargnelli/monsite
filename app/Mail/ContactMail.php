<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\contact;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contact = new Contact;
        $contact->name = $this->data['name'];
        $contact->email = $this->data['email'];
        $contact->msg = $this->data['body'];
        $contact->save();
        return $this->to('arthur@arthurcargnelli.eu')->from($contact->email)->subject('New Message')->view('mail.contact')->with('data', $this->data);
    }
}
