<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'albums';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $name = 'name';

    /**
     * The key presentation image associated with the table.
     *
     * @var string
     */
    protected $presentation_image = 'presentation_image';

    /**
     * show or not the album.
     *
     * @var string
     */
    protected $show = 'show';

    /**
     * Get the photos for the album.
     */
    public function images()
    {
        return $this->hasMany('App\photo');
    }

    public function images_count(){
        return $this->hasMany('App\photo')->count();
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $idType = 'string';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
