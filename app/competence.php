<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'competences';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $name = 'name';

    /**
     * The key logo associated with the table.
     *
     * @var string
     */
    protected $logo = 'logo';

     /**
     * Get the album that owns the photo.
     */
    public function projects()
    {
        return $this->belongsToMany('App\project','project_competence');
    }

    public function projects_count()
    {
        return $this->belongsToMany('App\project','project_competence')->count();
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $idType = 'string';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
