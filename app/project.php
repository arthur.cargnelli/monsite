<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $name = 'name';

    /**
     * The key description associated with the table.
     *
     * @var string
     */
    protected $description = 'description';

    /**
     * The key date start associated with the table.
     *
     * @var string
     */
    protected $img = 'img';
    /**
    * show or not the project.
     *
     * @var string
     */
    protected $show = 'show';

    /**
     * The key date end associated with the table.
     *
     * @var string
     */
    protected $url = 'url';

    public function competences()
    {
        return $this->belongsToMany('App\competence','project_competence')->withPivot('competence_id');
    }

    public function competences_count()
    {
        return $this->belongsToMany('App\competence','project_competence')->count();
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $idType = 'string';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
