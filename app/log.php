<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $ip = 'ip';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $browser = 'browser';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $os = 'os';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $page = 'page';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $city = 'city';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $region = 'region';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $country = 'country';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $latitude = 'latitude';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $longitude = 'longitude';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $time_zone = 'time_zone';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $idType = 'string';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
