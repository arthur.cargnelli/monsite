<?php

namespace app;

use Illuminate\Database\Eloquent\Model;
use App\newsCategory;
class news extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * The key presentation image associated with the table.
     *
     * @var string
     */
    protected $description = 'description';

    /**
     * The key presentation image associated with the table.
     *
     * @var string
     */
    protected $date = 'date';

    /**
     * The key presentation image associated with the table.
     *
     * @var string
     */
    protected $url = 'url';

    /**
     * Get the category that owns the news.
     */
    public function newsCategory()
    {
        return $this->belongsTo(newsCategory::class);
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $idType = 'string';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
