<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'experiences';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';

    /**
     * The key location associated with the table.
     *
     * @var string
     */
    protected $location = 'location';

    /**
     * The key compagny associated with the table.
     *
     * @var string
     */
    protected $compagny = 'compagny';

    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $name = 'name';

    /**
     * The key current associated with the table.
     *
     * @var bool
     */
    protected $current = 'current';

    /**
     * The key date start associated with the table.
     *
     * @var string
     */
    protected $date_start = 'date_start';

    /**
     * The key date end associated with the table.
     *
     * @var string
     */
    protected $date_end = 'date_end';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $idType = 'string';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
