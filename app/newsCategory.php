<?php

namespace app;

use Illuminate\Database\Eloquent\Model;
class newsCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news_category';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $name = 'name';

        /**
     * The key name associated with the table.
     *
     * @var string
     */
    protected $name_en = 'name_en';

    /**
     * Get the news of this category.
     */
    public function news()
    {
        return $this->hasMany('App\news');
    }

    public function news_count(){
        return $this->hasMany('App\news')->count();
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $idType = 'string';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
