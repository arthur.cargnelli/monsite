<?php
if (!function_exists('getServices')) {
    function convert($from, $to){
            $command = 'convert '
                . $from
                .' '
                . '-sampling-factor 4:2:0 -strip -quality 65'
                .' '
                . $to;
            return `$command`;
    }
}